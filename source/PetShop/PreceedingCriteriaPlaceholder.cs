using System;
using Training.DomainClasses;

public class PreceedingCriteriaPlaceholder<TItem>
{
    private readonly Func<Criteria<TItem>, Criteria<TItem>> _combineWithPreceedingCriteria;

    public PreceedingCriteriaPlaceholder(Func<Criteria<TItem>, Criteria<TItem>> combineWithPreceedingCriteria)
    {
        _combineWithPreceedingCriteria = combineWithPreceedingCriteria;
    }

    public FilteringEntryPoint<TItem, TField> hasA<TField>(Func<TItem, TField> fieldExtractor)
    {
        return new FilteringEntryPoint<TItem, TField>(fieldExtractor, _combineWithPreceedingCriteria);
    }
}