using System;
using System.Collections.Generic;
using Training.DomainClasses;

public static class FilteringEntryPointExtensions
{
    public static Criteria<TItem> EqualTo<TItem, TField>(this FilteringEntryPoint<TItem, TField> filteringEntryPoint, TField fieldValue)
    {
        var criteria = new AnonymousCriteria<TItem>(item => filteringEntryPoint.fieldExtractor(item).Equals(fieldValue));
        return filteringEntryPoint.ApplyContext(criteria);
    }
    public static Criteria<TItem> GreaterThan<TItem, TField>(this FilteringEntryPoint<TItem, TField> filteringEntryPoint, TField fieldValue) where TField : IComparable<TField>
    {
        var criteria = new AnonymousCriteria<TItem>(item => filteringEntryPoint.fieldExtractor(item).CompareTo(fieldValue) > 0);
        return filteringEntryPoint.ApplyContext(criteria);
    }

    public static Criteria<TItem> EqualToAny<TItem, TField>(this FilteringEntryPoint<TItem, TField> filteringEntryPoint, params TField[] allowedValues)
    {
        var allowedValuesList = new List<TField>(allowedValues);
        var criteria = new AnonymousCriteria<TItem>(item => allowedValuesList.Contains(filteringEntryPoint.fieldExtractor(item)));
        return filteringEntryPoint.ApplyContext(criteria);
    }

    public static FilteringEntryPoint<TItem, TField> Not<TItem, TField>(this FilteringEntryPoint<TItem, TField> filteringEntryPoint)
    {
//      //filteringEntryPoint.negate = !filteringEntryPoint.negate;
//      //return filteringEntryPoint;
        return filteringEntryPoint.Negate();
    }
}