using System;
using System.Collections.Generic;
using Training.DomainClasses;

public static class EnumerableExtensions
{
    public static IEnumerable<TItem> OneAtATime<TItem>(this IEnumerable<TItem> pets)
    {
        foreach (var pet in pets)
        {
            yield return pet;
        }
    }

    public static IEnumerable<Pet> ThatSatisfy(this IEnumerable<Pet> petsInTheStore, Predicate<Pet> predicate)
    {
        return petsInTheStore.ThatSatisfy(new AnonymousCriteria<Pet>(predicate));
    }
    public static IEnumerable<Pet> ThatSatisfy(this IEnumerable<Pet> petsInTheStore, Criteria<Pet> criteria)
    {
        foreach (var pet in petsInTheStore)
        {
            if (criteria.IsSatisfiedBy(pet))
                yield return pet;
        }
    }
}