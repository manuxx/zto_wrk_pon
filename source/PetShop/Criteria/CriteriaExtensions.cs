using Training.DomainClasses;

public static class CriteriaExtensions
{
    public static Criteria<TItem> And<TItem>(this Criteria<TItem> leftCriteria, Criteria<TItem> rightCriteria)
    {
        return new AndCriteria<TItem>(leftCriteria,rightCriteria);
    }

    public static PreceedingCriteriaPlaceholder<TItem> And<TItem>(this Criteria<TItem> leftCriteria)
    {
        return new PreceedingCriteriaPlaceholder<TItem>(leftCriteria.And);
    }

    public static Criteria<TItem> Or<TItem>(this Criteria<TItem> leftCriteria, Criteria<TItem> rightCriteria)
    {
        return new OrCriteria<TItem>(leftCriteria,rightCriteria);
    }

    public static PreceedingCriteriaPlaceholder<TItem> Or<TItem>(this Criteria<TItem> leftCriteria)
    {
        return new PreceedingCriteriaPlaceholder<TItem>(leftCriteria.Or);
    }
}