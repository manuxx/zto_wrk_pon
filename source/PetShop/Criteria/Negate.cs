namespace Training.DomainClasses
{
    public class Negate<TItem> : Criteria<TItem>
    {
        private readonly Criteria<TItem> _criteriaToNegate;

        public Negate(Criteria<TItem> criteriaToNegate)
        {
            _criteriaToNegate = criteriaToNegate;
        }

        public bool IsSatisfiedBy(TItem item)
        {
            return !_criteriaToNegate.IsSatisfiedBy(item);
        }
    }
}