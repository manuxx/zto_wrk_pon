using System;
using System.Collections.Generic;

namespace Training.DomainClasses
{
    public class FilteringEntryPoint<TItem, TField>
    {
        public readonly Func<TItem, TField> fieldExtractor;
        private readonly bool _negateNextCriteria;
        private readonly Func<Criteria<TItem>, Criteria<TItem>> _combineWithPreceedingCriteria;

        public FilteringEntryPoint(Func<TItem, TField> fieldExtractor) : this(fieldExtractor, false, DO_NOTHING) 
        {
        }

        public FilteringEntryPoint(Func<TItem, TField> fieldExtractor, Func<Criteria<TItem>, Criteria<TItem>> combineWithPreceedingCriteria)
            : this(fieldExtractor, false, combineWithPreceedingCriteria)
        {
        }

        public static Func<Criteria<TItem>, Criteria<TItem>> DO_NOTHING
        {
            get { return c => c; }
        }

        private FilteringEntryPoint(Func<TItem, TField> fieldExtractor, bool negateNextCriteria, Func<Criteria<TItem>, Criteria<TItem>> combineWithPreceedingCriteria)
        {
            this.fieldExtractor = fieldExtractor;
            _negateNextCriteria = negateNextCriteria;
            _combineWithPreceedingCriteria = combineWithPreceedingCriteria;
        }
        
        public FilteringEntryPoint<TItem, TField> Negate()
        {
            return new FilteringEntryPoint<TItem, TField>(fieldExtractor, ! _negateNextCriteria, _combineWithPreceedingCriteria);
        }

        public Criteria<TItem> ApplyContext(Criteria<TItem> nextCriteria)
        {
            var result = nextCriteria;
            if (_negateNextCriteria)
            {
                result = new Negate<TItem>(result);
            }
            if(_combineWithPreceedingCriteria != null)
            {
                result = _combineWithPreceedingCriteria(result);
            }
            return result;
        }
    }
}