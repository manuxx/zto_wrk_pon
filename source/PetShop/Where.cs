using System;

namespace Training.DomainClasses
{
    public class Where<TItem>
    {
        public static FilteringEntryPoint<TItem,TField> hasA<TField>(Func<TItem, TField> fieldExtractor)
        {
            return new FilteringEntryPoint<TItem,TField>(fieldExtractor);
        }
    }
}